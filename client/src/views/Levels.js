import * as React from 'react';
import LevelsProvider, { LevelsConsumer } from '../components/LevelsProvider';
import TableBody from '@material-ui/core/TableBody';
import PlanningSelector from '../components/PlanningSelector';
import DemandTableSelector from '../components/DemandTableSelector';
import SupplyTableSelector from '../components/SupplyTableSelector';
import LevelsList from '../components/LevelsList';
import LevelsItem from '../components/LevelsItem';

const Levels = () => (
  <div className="container">
    <LevelsProvider>
      <LevelsConsumer>
        {({ planningId, demandTableId, supplyTableId, updateContext }) => (
          <React.Fragment>
            <div className="selectors">
              <PlanningSelector
                updateContext={updateContext}
              />
              <DemandTableSelector
                updateContext={updateContext}
                planningId={planningId}
              />
              <SupplyTableSelector
                updateContext={updateContext}
                planningId={planningId}
              />
            </div>
            <LevelsList
              demandTableId={demandTableId}
              supplyTableId={supplyTableId}
            >
              {({ demandLevels, supplyLevels, fields, names }) => (
                <TableBody>
                  {
                    names.map((levelName, levelNameIdx) => (
                      <LevelsItem
                        key={`${levelName}${levelNameIdx}`}
                        demandLevels={demandLevels}
                        supplyLevels={supplyLevels}
                        fields={fields}
                        names={names}
                        levelName={levelName}
                        levelNameIdx={levelNameIdx}
                      />
                    ))
                  }
                </TableBody>
              )}
            </LevelsList>
          </React.Fragment>
        )}
      </LevelsConsumer>
    </LevelsProvider>
  </div>
);

export default Levels;
