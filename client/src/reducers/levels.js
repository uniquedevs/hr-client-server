import { combineReducers } from 'redux';

const addLevelEntry = (state, action) => {
  const { payload } = action;
  const { levelId } = payload;
  return {
    ...state,
    [levelId]: payload
  };
};

const levelsById = (state = {}, action) => {
  switch (action.type) {
    case 'UPDATE_LEVEL':
      return addLevelEntry(state, action);
    default:
      return state;
  }
};

const addLevelId = (state, action) => {
  const { payload } = action;
  const { levelId } = payload;
  return state.concat(levelId);
};

const allLevels = (state = [], action) => {
  switch (action.type) {
    case 'UPDATE_LEVEL':
      return addLevelId(state, action);
    default:
      return state;
  }
};


export default combineReducers({
  byId: levelsById,
  ids: allLevels
});
