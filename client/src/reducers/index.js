import { combineReducers } from 'redux';
import levels from './levels';
import tables from './tables';
import plannings from './plannings';

export default combineReducers({
  levels,
  tables,
  plannings,
});
