import { combineReducers } from 'redux';


const addTableEntry = (state, action) => {
  const { payload } = action;
  const { tableId } = payload;
  return {
    ...state,
    [tableId]: payload
  };
};

function addLevel(state, action) {
  const { payload } = action;
  const { tableId, levelId } = payload;
  // Look up the correct table, to simplify the rest of the code
  const table = state[tableId];

  return {
    ...state,
    // Update our Table object with a new "planningLevels" array
    [tableId]: {
      ...table,
      planningLevels: table.planningLevels.concat(levelId)
    }
  };
}

const TablesById = (state = {}, action) => {
  switch (action.type) {
    case 'UPDATE_TABLE':
      return addTableEntry(state, action);
    case 'UPDATE_LEVEL':
      return addLevel(state, action);
    default:
      return state;
  }
};

const addTableId = (state, action) => {
  const { payload } = action;
  const { tableId } = payload;
  return state.concat(tableId);
};

const allTables = (state = [], action) => {
  switch (action.type) {
    case 'UPDATE_TABLE':
      return addTableId(state, action);
    default:
      return state;
  }
};


export default combineReducers({
  byId: TablesById,
  ids: allTables
});
