import { combineReducers } from 'redux';

const addPlanningEntry = (state, action) => {
  const { payload } = action;
  const { planningId } = payload;
  return {
    ...state,
    [planningId]: payload
  };
};

const updateTable = (state, action) => {
  const { payload } = action;
  const { planningId, planningType, tableId } = payload;
  const planning = state[planningId];
  return {
    ...state,
    [planningId]: {
      ...planning,
      [planningType]: planning[planningType].concat(tableId)
    }
  };
};

const planningById = (state = {}, action) => {
  switch (action.type) {
    case 'UPDATE_PLANNING':
      return addPlanningEntry(state, action);
    case 'UPDATE_TABLE':
      return updateTable(state, action);
    default:
      return state;
  }
};

const addPlanningId = (state, action) => {
  const { payload } = action;
  const { planningId } = payload;
  return state.concat(planningId);
};

const allPlannings = (state = [], action) => {
  switch (action.type) {
    case 'UPDATE_PLANNING':
      return addPlanningId(state, action);
    default:
      return state;
  }
};


export default combineReducers({
  byId: planningById,
  ids: allPlannings
});
