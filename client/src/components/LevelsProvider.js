import * as React from 'react';

const DefaultState = {
  planningId: null,
  demandTableId: null,
  supplyTableId: null,
};

const LevelsContext = React.createContext(DefaultState);

export const LevelsConsumer = LevelsContext.Consumer;

class LevelsProvider extends React.Component {
  state = DefaultState;

  updateContext = selector => {
    this.setState(selector);
  };

  render() {
    const { children } = this.props;

    return (
      <LevelsContext.Provider
        value={{
          ...this.state,
          updateContext: this.updateContext
        }}
      >
        {children}
      </LevelsContext.Provider>
    );
  }
}

export default LevelsProvider;
