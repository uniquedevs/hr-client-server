import React from 'react';
import Level from './Level';

export default ({ demandLevels, supplyLevels, fields, levelName, levelNameIdx }) => {
  const calcFields = fields.slice(1);
  const mapGapLevel = (level, idx) => ({
    ...Object.keys(level).reduce((acc, field) => ({
      ...acc,
      [field]: !!~calcFields.indexOf(field)
        ? level[field] - supplyLevels[idx][field]
        : level[field]
    }), {})
  });
  const [visible, toggleVisible] = React.useState(false);
  const gapLevels = (
    demandLevels.length && supplyLevels.length
      ? demandLevels.map(mapGapLevel)
      : []
  );
  const demandLevelVisible =
    (demandLevels.length && !gapLevels.length) || (demandLevels.length && visible);

  const supplyLevelVisible =
    (supplyLevels.length && !gapLevels.length) || (supplyLevels.length && visible);

  function toggleDataVisibility() {
    toggleVisible(visible => visible =!visible);
  }
  return (
    <React.Fragment key={`${levelName}-${levelNameIdx}`}>
          {
            gapLevels.length
            ? (
              <Level
                level={gapLevels[levelNameIdx]}
                fields={calcFields}
                levelName={`${levelName}`}
                isgap
                onToggle={toggleDataVisibility}
              />
            )
            : null
          }
          {
            demandLevelVisible
            ? (
              <Level
                level={demandLevels[levelNameIdx]}
                fields={calcFields}
                levelName={`${levelName} (demand)`}
              />
            )
            : null
          }
          {
            supplyLevelVisible
              ? (
                <Level
                  level={supplyLevels[levelNameIdx]}
                  fields={calcFields}
                  levelName={`${levelName} (supply)`}
                />
              )
              : null
          }
    </React.Fragment>
  );
};
