import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


const map = {
  levelName: 'Planning Name'
};

const levelNamesMapper = name => map[name] || name.replace('_', ' ');

const LevelListHeader = ({ cols }) => (
  <TableHead>
    <TableRow>
    {cols.map((colName, id) => (
      <TableCell key={`${colName}${id}`}>
        {levelNamesMapper(colName)}
      </TableCell>
    ))}
    </TableRow>
  </TableHead>
);

export default LevelListHeader;
