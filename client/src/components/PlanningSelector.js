/* eslint-disable jsx-a11y/label-has-associated-control */
import * as React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
// import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';

import { getPlanningOptions } from '../selectors/plannings';


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 400,
  },
}));

const PlanningSelect = ({ options, updateContext }) => {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    planningId: '',
  });
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);

  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
    updateContext(values);
  }, [updateContext, values]);

  React.useEffect(() => {
    if (options.length) {
      setValues(oldValues => ({
        ...oldValues,
        planningId: options[0].id
      }));
    }
  }, [options]);

  function handleChange(prop, value) {
    setValues(oldValues => ({
      ...oldValues,
      [prop]: value
    }));
  }

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel ref={inputLabel} htmlFor="plannings">
        Plan your data
      </InputLabel>
      <Select
        value={values.planningId}
        onChange={event => handleChange('planningId', event.target.value)}
        input={<OutlinedInput labelWidth={labelWidth} name="plannings" id="plannings" />}
      >
        {options.map(({ name, id }) => (
          <MenuItem value={id} key={id}>{name}</MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

PlanningSelect.propTypes = {
  updateContext: PropTypes.func,
  options: PropTypes.array
};

const getProps = createStructuredSelector({
  options: getPlanningOptions,
});

const merge = (state, handlers, own) => ({
  ...state, ...handlers, ...own,
});

export default connect(getProps, null, merge)(PlanningSelect);
