import React from 'react';
import { makeStyles } from '@material-ui/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import SvgIcon from '@material-ui/core/SvgIcon';


const useStyles = makeStyles({
  foo: props => ({
    fontWidth: '700',
    backgroundColor: 'Ivory',
  }),
});

export default ({ level, fields, levelName, isgap, onToggle }) => {
  const classes = useStyles();
  return (
    <TableRow>
      <TableCell
        className={`${isgap && classes.foo}`}
        onClick={() => typeof onToggle === 'function' && onToggle()}
      >
        {levelName}
        {isgap ? (<SvgIcon><path d="M7 10l5 5 5-5z"/></SvgIcon>) : null}
      </TableCell>
      <React.Fragment>
        {fields.map((field, id) => (
          <TableCell
            key={`${field}-${id}`}
            className={`${isgap && classes.foo}`}
          >{level[field]}</TableCell>
        ))}
      </React.Fragment>
    </TableRow>
  );
};
