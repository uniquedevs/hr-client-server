/* eslint-disable jsx-a11y/label-has-associated-control */
import * as React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { getSupplyOptions } from '../selectors/tables';


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 400,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const usePrevious = (value) => {
  const ref = React.useRef();
  React.useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

const useCompare = (val) => {
  const prevVal = usePrevious(val);
  return prevVal !== val
};

const SupplySelect = ({ options, updateContext, planningId }) => {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    supplyTableId: '',
  });
  const hasPalnningIdChanged = useCompare(planningId);

  React.useEffect(() => {
    if (hasPalnningIdChanged) {
      setValues(oldValues => ({
        ...oldValues,
        supplyTableId: ''
      }));
    }
  }, [hasPalnningIdChanged]);

  React.useEffect(() => {
    updateContext(values);
  }, [updateContext, values]);

  function handleChange(prop, value) {
    setValues(oldValues => ({
      ...oldValues,
      [prop]: value
    }));
  }

  return (
    <FormControl className={classes.formControl}>
      <InputLabel htmlFor="supply">Supply Scenario</InputLabel>
      <Select
        value={values.supplyTableId}
        onChange={event => handleChange('supplyTableId', event.target.value)}
        inputProps={{
          name: 'supply',
          id: 'supply',
        }}
      >
        <MenuItem value="" key="default">
          <em>Choose...</em>
        </MenuItem>
        {options.map(({ name, id }) => (
          <MenuItem value={id} key={id}>{name}</MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

SupplySelect.propTypes = {
  // from props
  updateContext: PropTypes.func,
  planningId: PropTypes.string,
  // form state
  options: PropTypes.array,
};

const getProps = createStructuredSelector({
  options: getSupplyOptions,
});

const merge = (state, handlers, own) => ({
  ...state, ...handlers, ...own,
});

export default connect(getProps, null, merge)(SupplySelect);
