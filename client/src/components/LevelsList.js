import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Paper from '@material-ui/core/Paper';

import LevelListHeader from './LevelListHeader';
import { getLevelsByTableId } from '../selectors/levels';


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
}));

const mapFields = ({ levelId, tableId, ...fields }) => fields;

const getLevelFields = (...levels) => (
  Object.keys([].concat.apply(...levels)[0] || [])
);

const pipe = (value, ...funcs) => funcs.reduce(
  (a, f) => f(a), value
);

const ensureNotEmpty = levelsCollection => levelsCollection.find(levels => levels.length);
const ensureLevelNames = levels => (levels ? levels.map(level => level.levelName) : []);

const getLevelNames = (...levels) => pipe(
  levels,
  ensureNotEmpty,
  ensureLevelNames,
);

const LevelsList = ({ demandLevels, supplyLevels, children }) => {
  const classes = useStyles();
  demandLevels = demandLevels.map(mapFields);
  supplyLevels = supplyLevels.map(mapFields);
  const fields = getLevelFields(demandLevels, supplyLevels);
  const names = getLevelNames(demandLevels, supplyLevels);
  return (
    <Paper className={classes.root}>
      { demandLevels.length || supplyLevels.length
        ? (
          <Table>
            <LevelListHeader
              cols={fields}
            />
            {children({ demandLevels, supplyLevels, names, fields })}
          </Table>
        )
        : (
          <div>
            Please, choose a table
          </div>
        )
      }
    </Paper>
  )};

LevelsList.propTypes = {
  // from props
  demandTableId: PropTypes.string,
  supplyTableId: PropTypes.string,
  // from state
  supplyLevels: PropTypes.array,
  demandLevels: PropTypes.array,
};

const getProps = (state, { demandTableId, supplyTableId }) => ({
  supplyLevels: getLevelsByTableId(state, { tableId: supplyTableId }),
  demandLevels: getLevelsByTableId(state, { tableId: demandTableId }),
});

export default connect(getProps)(LevelsList);
