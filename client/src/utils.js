export const pipe = (value, ...funcs) => funcs.reduce(
  (a, f) => f(a), value
);
