import React, { Component } from 'react';
import './App.css';

import { connect } from 'react-redux';
import { planningToStore } from './actions';
// import collection from './collection';
import Levels from './views/Levels';



class App extends Component {
  componentWillMount() {
    const { dispatch } = this.props;
    fetch('http://localhost:3001/api/plannings')
      .then(data => data.json())
      .then(collection => collection.map(dispatch(planningToStore)));
  }

  render() {
    return (
      <div className="App">
        <Levels />
      </div>
    );
  }
}

export default connect()(App);
