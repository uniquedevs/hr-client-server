import { createSelector } from 'reselect';

export const getPlannings = state => state.plannings.byId;
const getPlanningsIds = state => state.plannings.ids;

export const getPlanningOptions = createSelector(
  [getPlannings, getPlanningsIds],
  (all, ids) => ids.reduce(
    (acc, id) => {
      return acc.concat([{ name: all[id].planningName, id: all[id].planningId }]);
    }, [])
);
