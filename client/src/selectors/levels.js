import { createSelector } from 'reselect';
import { getTables } from './tables';

const getLevels = state => state.levels.byId;
// const getLevelsIds = state => state.levels.ids;

const getSelectedTableId = (_, { tableId }) => tableId;

export const getLevelIdsByTableId = createSelector(
  [getTables, getSelectedTableId],
  (tables, tableId) => tableId && tables[tableId].planningLevels
);

export const getLevelsByTableId = createSelector(
  [getLevels, getLevelIdsByTableId],
  (levels, ids) => (ids ? ids.map(id => levels[id]) : [])
);
