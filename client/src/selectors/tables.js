import { createSelector } from 'reselect';

import { getPlannings } from './plannings';


export const getTables = state => state.tables.byId;
// const tablesIds = state => state.tables.ids;

const getSelectedPlanningId = (_, { planningId }) => planningId;

const getSelectedDemandTablesIds = createSelector(
  [getPlannings, getSelectedPlanningId],
  (plannings, planningId) => (
    planningId ? plannings[planningId].planningDemand : []
  )
);

const getSelectedSupplyTablesIds = createSelector(
  [getPlannings, getSelectedPlanningId],
  (plannings, planningId) => (
    planningId ? plannings[planningId].planningSupply : []
  )
);

export const getDemandOptions = createSelector(
  [getTables, getSelectedDemandTablesIds],
  (all, ids) => ids.reduce(
    (acc, id) => {
      return acc.concat([{ name: all[id].tableName, id: all[id].tableId }]);
    }, [])
);

export const getSupplyOptions = createSelector(
  [getTables, getSelectedSupplyTablesIds],
  (all, ids) => ids.reduce(
    (acc, id) => {
      return acc.concat([{ name: all[id].tableName, id: all[id].tableId }]);
    }, [])
);
