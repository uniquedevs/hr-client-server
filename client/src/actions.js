import { pipe } from './utils';

export const updateLevel = payload => ({
  type: 'UPDATE_LEVEL',
  payload
});

export const updateTable = payload => console.log(payload) || ({
  type: 'UPDATE_TABLE',
  payload
});

export const updatePlanning = payload => ({
  type: 'UPDATE_PLANNING',
  payload
});

const levelToStore = tableId => dispatch => level => {
  level.tableId = tableId;
  dispatch(updateLevel(level));
};

const enrichTable = (planningId, planningType) => table => ({
  ...table,
  planningLevels: [],
  planningId,
  planningType,
});

const pureTableToStore = (table, planningId, planningType) => dispatch => pipe(
  table,
  enrichTable(planningId, planningType),
  updateTable,
  dispatch
);

const mapTableToStore = (planningId, planningType) => table => dispatch  => {
  const { planningLevels, ...pureTable } = table;
  dispatch(pureTableToStore(pureTable, planningId, planningType));
  planningLevels.map(dispatch(levelToStore(table.tableId)));
};

const tableToStore = planningType => planningId => dispatch => table => pipe(
  table,
  mapTableToStore(planningId, planningType),
  dispatch
);

const supplyTableToStore = tableToStore('planningDemand');
const demandTableToStore = tableToStore('planningSupply');

const enrichPlanning = planning => ({
  ...planning,
  planningDemand: [],
  planningSupply: [],
});

const purePlanningToStore = planning => dispatch => pipe(
  planning,
  enrichPlanning,
  updatePlanning,
  dispatch
);

export const planningToStore = dispatch => planning => {
  const { planningDemand, planningSupply, ...purePlanning } = planning;
  dispatch(purePlanningToStore(purePlanning));
  planningDemand.map(dispatch(supplyTableToStore(planning.planningId)));
  planningSupply.map(dispatch(demandTableToStore(planning.planningId)));
};
