const request = require('supertest');
const app = require('./server');

describe('GET /plannings', () => {
  it('respond with json containing a list of all plannings',
    (done) => {
      request(app)
        .get('/api/plannings')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });
});
