// it should then send back jsonData on a GET to /data
const express = require('express');
const crossdomain = require('./middleware/crossdomain');
const api = require('./api/api');
const autoPopulate = require('./autoPopulate');
const config = require('./config/config');

const app = express();

app.use(crossdomain());

app.use('/api/', api);

if (config.seed) autoPopulate();


module.exports = app;
