const config = {
  env: process.env.node || 'development',
  db: 'mongodb://localhost/hr',
  serverPort: 3001,
  expireTime: 100 * 60 * 60,
  seed: true,
  baseUrl: 'http://localhost:3000/',
};

let envConfig;

try {
  envConfig = require('./' + config.env);
} catch (err) {
  envConfig = {}
}

module.exports = Object.assign(config, envConfig);
