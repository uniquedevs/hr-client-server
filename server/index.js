const mongoose = require('mongoose');
const app = require('./server');
const config = require('./config/config');


mongoose.connect(config.db, { useNewUrlParser: true });

app.listen(config.serverPort, () => console.log('listenning port 3001'));
