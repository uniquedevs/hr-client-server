module.exports = () => (err, req, res) => {
  if (err) {
    console.log(err.stack);
    console.log('NAME', err.name);
    // mongo error
    if (err.name === 'MongoError') {
      return res.status(400).send({
        errors: [
          { name: 'mongoError', msg: err.errmsg }
        ]
      });
    }
    // mongoose validation error
    if (err.name === 'ValidationError') {
      const errors = Object.keys(err.errors).map(key => {
        const { message: msg, name, value, path: param } = err.errors[key];
        return { msg, name, value, param };
      });
      return res.status(err.status || 400).send({
        errors
      });
    }
    // other errors
    res.status(err.status || 500).send({
      errors: err.errors || [{ msg: err.message || 'server error' }]
    });
  }
};
