const mongoose = require('mongoose');

const LevelSchema = new mongoose.Schema({
  levelName: {
    type: String,
    required: true,
  }
},
{
  strict: false,
  versionKey: false,
  toJSON: {
    transform(doc, ret) {
      ret.levelId = ret._id;
      delete ret._id;
    }
  }
});

const Level = mongoose.model('level', LevelSchema);

module.exports = Level;
