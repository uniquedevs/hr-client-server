const router = require('express').Router();
const planningRouter = require('./plannings/planningRouter');

router.use('/plannings', planningRouter);

module.exports = router;
