const router = require('express').Router();

const controller = require('./planningCtrl');

router.route('/')
  .get(controller.get);

module.exports = router;
