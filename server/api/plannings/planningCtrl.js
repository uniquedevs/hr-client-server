const Planning = require('./planningModel');
const Table = require('../tables/tableModel');
const Level = require('../levels/levelModel');

const populateTables = (err, plannings) => {
  Table.populate(plannings, {
    path: 'planningLevels',
    model: Level
  }, (error, populatedPlannings) => {
    return populatedPlannings;
  });
};

const cons = (err, col) => {
  return Promise.resolve(col);
};

module.exports = {
  get(req, res) {
    Planning.find({})
      .populate([
        {
          path: 'planningDemand',
          model: Table,
          populate: {
            path: 'planningLevels',
            model: Level,
          }
        },
        {
          path: 'planningSupply',
          model: Table,
          populate: {
            path: 'planningLevels',
            model: Level,
          }
        },
      ])
      .exec()
      .then(plannings => res.json(plannings));
  },
};
