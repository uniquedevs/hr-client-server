const mongoose = require('mongoose');

const PlanningSchema = new mongoose.Schema({
  planningName: {
    type: String,
    required: true,
  },
  planningDemand: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'table',
  }],
  planningSupply: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'table',
  }]
},
{
  versionKey: false,
  // toObject: {
  //   transform(doc, ret) {
  //     ret.id = ret._id;
  //     delete ret._id;
  //   }
  // },
  toJSON: {
    transform(doc, ret) {
      ret.planningId = ret._id;
      delete ret._id;
    }
  }
});

PlanningSchema.methods.toBackbone = function () {
  const obj = this.toObject();
  obj.id = obj._id;
  delete obj._id;
  return obj;
};

const Planning = mongoose.model('planning', PlanningSchema);

module.exports = Planning;
