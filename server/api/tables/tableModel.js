const mongoose = require('mongoose');

const TableSchema = new mongoose.Schema({
  tableName: {
    type: String,
    required: true,
  },
  planningLevels: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'level',
  }]
},
{
  versionKey: false,
  toJSON: {
    transform(doc, ret) {
      ret.tableId = ret._id;
      delete ret._id;
    }
  }
});

const Table = mongoose.model('table', TableSchema);

module.exports = Table;
