const Planning = require('./api/plannings/planningModel');
const Table = require('./api/tables/tableModel');
const Level = require('./api/levels/levelModel');
const collection = require('./mock_collection');

const autoPopulate = () => {
  Planning.deleteMany({})
    .then(() => Table.deleteMany({}))
    .then(() => Level.deleteMany({}))
    .then(() => {
      for (const planning of collection) {
        const p = new Planning({
          planningName: planning.planningName,
          planningDemand: [],
          planningSupply: [],
        });
        Planning.create(p);
        for (const table of planning.planningDemand) {
          const t = new Table({
            tableName: table.tableName,
            planningLevels: []
          });
          Table.create(t);
          p.planningDemand.push(t);
          for (const level of table.planningLevels) {
            const l = new Level(level);
            Level.create(l);
            t.planningLevels.push(l);
          }
        }
        for (const table of planning.planningSupply) {
          const t = new Table({
            tableName: table.tableName,
            planningLevels: []
          });
          Table.create(t);
          p.planningSupply.push(t);
          for (const level of table.planningLevels) {
            const l = new Level(level);
            Level.create(l);
            t.planningLevels.push(l);
          }
        }
      }
    });
};

module.exports = autoPopulate;
